#include<iostream>
using namespace std;
//Switch case instead of if-else condition with simple condition
int main()
{
    int n=10;
    // cin >> n;

    switch (n)
    {
    case 1:
        cout << "One" << endl;
        break;
    case 2:
        cout << "Two" << endl;
        break;
    case 3:
        cout<< "Three" << endl;
        break;
    case 4:
        cout << "Four" << endl;
        break;
    default:
        cout<< "Nothing similar" << endl;
    }
    return 0;
}