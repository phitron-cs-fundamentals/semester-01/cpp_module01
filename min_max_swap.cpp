#include<iostream>
#include<algorithm> //min & max header file
#include<utility> //swap header file

using namespace std;

// void my_swap(int *a, int *b) //swap function
// {
//     int tmp = *a;
//     *a=*b;
//     *b=tmp;
// }

int main()
{
    int a, b;
    cin >> a >> b; 
    // my_swap(&a, &b); //pointer for swapping
    swap(a,b);
    cout << a << " " <<b << endl;


    // int c = min (a, b);
    // int d = max(a, b);
    // cout << c << " " << d << endl;
    // int mn = min({a, b, c, d});
    // int mx = max({a, b, c, d});
    // cout << mn << " " << mx << endl;



    return 0;
}