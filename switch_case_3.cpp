#include<iostream>
using namespace std;
//Switch case instead of if-else condition with simple condition
int main()
{
    char n;
    cin >> n;
    switch(n)
    {
        case 'a':
            cout<< "Vowel";
            break;
        case 'e':
            cout<< "Vowel";
            break;
        case 'i':
            cout<< "Vowel";
            break;
        case 'o':
            cout<< "Vowel";
            break;
        case 'u':
            cout<< "Vowel";
            break;
        default:
            cout<< "Consonent";
            break;
    }
    
    return 0;
}